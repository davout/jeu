#CFLAGS=-g -std=gnu99 -Wall -Wextra -Isrc -DNDEBUG $(OPTFLAGS)
# CXX=g++-6
# CC=gcc-6
CPPFLAGS=-g -Wall -Wextra -Isrc -DNDEBUG $(OPTFLAGS)
LDFLAGS=-lSDL2 -lSDL2_Mixer -lSDL2_Image -lSDL2_ttf

SOURCES=$(wildcard src/**/*.cpp src/*.cpp)
OBJECTS=$(patsubst %.cpp,%.o,$(SOURCES))

TARGET=build/libjeu.a
SO_TARGET=$(patsubst %.a,%.so,$(TARGET))

all: $(TARGET) jeu

jeu: CFLAGS += $(TARGET)
jeu:
	$(CXX) jeu.cpp -o jeu $(CFLAGS) $(CPPFLAGS) $(LDFLAGS)

$(TARGET): CPPFLAGS += -fPIC
$(TARGET): build $(OBJECTS)
	ar rcs $@ $(OBJECTS)
	ranlib $@

build:
	@mkdir -p build

clean:
	@rm -rf build jeu $(OBJECTS)
	@rm -rf `find . -name "*.dSYM" -print`
