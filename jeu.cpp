#include "jeu.hpp"

int main(void)
{
	int quit = 0;
	uint32_t tick_delta;
	SDL_Event event;
	SDL_Rect world_box;
	Mix_Music* background_music;

	srand((uint32_t)time(NULL));

	// Initialize keyboard
	keyboard = SDL_GetKeyboardState(NULL);

	// Initialize view
	view = new View();
	world_box = view->getBox();

	// Get initial tick count
	ticks = SDL_GetTicks();

	// Start music background
	Sound_Init();
	background_music = Sound_LoadMusic(MUS_MAIN_LOOP);
	Sound_PlayMusic(background_music);

	// Load SFX
	sfx_catch = Sound_Load(SFX_CORRECT);
	sfx_fall = Sound_Load(SFX_LAUGHTER_03);

	// Set up player
	player = new Player();
	addToGameObjectsCollection(player);

	// Set up baby
	baby = new Baby();
	addToGameObjectsCollection(baby);

	// Set up score
	SDL_Point position = { INT_SCORE_X_OFFSET, INT_SCORE_Y_OFFSET };

	score_label = new TextLabel("", STR_FONT_FACE, INT_FONT_SIZE,
			position, view->getRenderer());

	addToGameObjectsCollection(score_label);

	// Set up lives
	position.x = 800;
	lives_label = new TextLabel("Vies : 3", STR_FONT_FACE, INT_FONT_SIZE,
			position, view->getRenderer());

	addToGameObjectsCollection(lives_label);

	while (!quit)
	{
		tick_delta = SDL_GetTicks() - ticks;
		ticks += tick_delta;

		while (SDL_PollEvent(&event) != 0)
			if (event.type == SDL_QUIT)
			{
				printf("Quitting game...\n");
				quit = 1;
			}

		view->reset();

		handleKeyboardState();
		spawnToys();

		for (int i = 0; i < n_game_objects; i++)
			game_objects[i]->updatePhysics(tick_delta);

		for (int i = 0; i < n_game_objects; i++)
			game_objects[i]->updateGraphics(tick_delta);

		HandleFallenAndCollidingToys();
		updateScore();

		view->show();
	}

	return 0;
}

/**
 * Dispatch keyboard events to relevant GameObjects
 */
void handleKeyboardState()
{
	SDL_PumpEvents();
	player->handleInput(keyboard);
}

void spawnToys()
{
	GameObject* toy;

	// Set initial baby position
	if (last_spawn == 0)
	{
		SDL_Rect baby_box = baby->getBox();
		next_spawn_x = rand() % SCREEN_WIDTH;
		baby_box.x = next_spawn_x;
		baby->setBox(baby_box);
	}

	if ((last_spawn == 0) || ((SDL_GetTicks() - last_spawn) >= INT_SPAWN_DELAY))
	{
		last_spawn = SDL_GetTicks();

		toy = new Toy(next_spawn_x);
		addToGameObjectsCollection(toy);

		// Start moving baby to next spawn point
		next_spawn_x = rand() % SCREEN_WIDTH;

		baby->travelTo(next_spawn_x, INT_SPAWN_DELAY);
	}
}

void HandleFallenAndCollidingToys()
{
	GameObject* toy;
	SDL_Rect toy_rect;
	int destroyed_toys = 0;

	for (int i = 4; i < n_game_objects; i++)
	{
		toy = game_objects[i - destroyed_toys];

		if (toy != NULL)
		{
			toy_rect = toy->getBox();

			if (player->isBoxCollidingWith(toy))
			{
				score += 100;
				Sound_Play(sfx_catch);
				removeFromGameObjectCollection(toy);
				destroyed_toys++;
			}
			else if (toy_rect.y + toy_rect.h >= SCREEN_HEIGHT)
			{
				// Take a life
				Sound_Play(sfx_fall);
				removeFromGameObjectCollection(toy);
				destroyed_toys++;
			}
		}
	}
}

void addToGameObjectsCollection(GameObject* game_object)
{
	game_objects[n_game_objects++] = game_object;
}

void removeFromGameObjectCollection(GameObject* game_object)
{
	int idx = 0;

	while (game_objects[idx] != game_object && idx <= n_game_objects) idx++;

	if (idx == n_game_objects)
	{
		printf("Unable to find provided sprite in this view's collection\n");
		exit(1);
	}

	game_objects[idx] = NULL;

	// Compact the objects array
	for (int i = idx; i < (n_game_objects - 1); i++)
	{
		game_objects[i] = game_objects[i + 1];
		game_objects[i + 1] = NULL;
	}

	// Decrement total Sprite count
	n_game_objects--;

	delete game_object;
}

void updateScore()
{
	char tmp_score[9];
	sprintf(tmp_score, "%08d", score);
	std::string str_score = std::string(tmp_score);
	score_label->update(str_score);
}

