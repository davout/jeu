#ifndef _jeu_h
#define _jeu_h

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "src/sdl.hpp"
#include "src/constants.hpp"
#include "src/view.hpp"
#include "src/sound.hpp"
#include "src/player.hpp"
#include "src/baby.hpp"
#include "src/toy.hpp"

// Main music loop
static const char* MUS_MAIN_LOOP = "main-loop.ogg";

// Sound effects
static const char* SFX_CORRECT = "correct.wav";
static const char* SFX_LAUGHTER_03 = "laughter_03.wav";

// Score
static const char* STR_FONT_FACE = "Walkway_UltraBold.ttf";
static const int INT_FONT_SIZE = 40;
static const int INT_SCORE_X_OFFSET = 15;
static const int INT_SCORE_Y_OFFSET = 15;

static const uint8_t* keyboard;
static uint32_t ticks;
static Player* player;
static Baby* baby;
static TextLabel* score_label;
static TextLabel* lives_label;
static int score = 0;
static GameObject* game_objects[0xFF];
static int n_game_objects = 0;

static View* view;
static int next_spawn_x;
static uint32_t last_spawn = 0;

static Mix_Chunk* sfx_catch;
static Mix_Chunk* sfx_fall;

void handleKeyboardState();
void spawnToys();
void HandleFallenAndCollidingToys();
void addToGameObjectsCollection(GameObject* game_object);
void removeFromGameObjectCollection(GameObject* game_object);
void compactGameObjectCollection();
void updateScore();

#endif

