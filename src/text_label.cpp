#include "text_label.hpp"

// SDL colors
static SDL_Color BLACK = { 0x00, 0x00, 0x00, 0x00 };

/* Has SDL_ttf already been initialized */
bool TextLabel::ttf_initialized = false;

/**
 * Creates a `TextLabel` instance.
 */
TextLabel::TextLabel(std::string label, std::string font_name, int font_size,
		SDL_Point position, SDL_Renderer* renderer) :
	GameObject()
{
	initTTF();

	label_ = label;
	rect_.x = position.x;
	rect_.y = position.y;
	renderer_ = renderer;

	font_ = NULL;
	font_ = TTF_OpenFont(Util::resolveResourcePath(RESOURCE_FONT,
				font_name).c_str(), font_size);

	if(!font_)
		Util::die("TTF_OpenFont failed to load font", TTF_GetError());

	renderTexture();
}

/**
 * Finalize `TextLabel` instance
 */
TextLabel::~TextLabel()
{
	SDL_DestroyTexture(texture_);
}

/**
 * Draws the text
 */
void TextLabel::updateGraphics(uint32_t tick_delta)
{
	SDL_RenderCopy(renderer_, texture_, NULL, &rect_);
}

/**
 * Initialize fonts and text rendering
 */
void TextLabel::initTTF()
{
	if (!ttf_initialized)
	{
		if (TTF_Init() == -1)
			Util::die("SDL_ttf could not initialize", SDL_GetError());

		ttf_initialized = true;
	}
}

/**
 * Renders the label as a texture for later drawing
 */
void TextLabel::renderTexture()
{
	int text_width = 0, dummy, tmp_width;
	const char* tmp_c_label = label_.c_str();

	SDL_Surface* surface;
	surface = TTF_RenderText_Solid(font_, tmp_c_label, BLACK);
	texture_ = SDL_CreateTextureFromSurface(renderer_, surface);
	SDL_FreeSurface(surface);

	for (int i = 0; tmp_c_label[i] != '\0'; i++)
	{
		TTF_GlyphMetrics(font_, tmp_c_label[i], &dummy, &dummy, &dummy, &dummy,
				&tmp_width);

		text_width += tmp_width;
	}

	rect_.h = TTF_FontHeight(font_);
	rect_.w = text_width;
}

/**
 * Changes the underlying label string and re-renders the texture, unless the
 * provided text is the same as the one we already have.
 */
void TextLabel::update(std::string label)
{
	if (label != label_)
	{
		label_ = label;
		SDL_DestroyTexture(texture_);
		renderTexture();
	}
}

