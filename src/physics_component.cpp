#include "physics_component.hpp"
#include "game_object.hpp"

PhysicsComponent::PhysicsComponent(GameObject* game_object, 
		SDL_Rect bounding_box) :
	game_object_(game_object),
	bounding_box_(bounding_box)
{
	// Initialize velocity
	velocity_.x = 0.0;
	velocity_.y = 0.0;

	box_ = game_object->getBox();
}

PhysicsComponent::~PhysicsComponent()
{
}

void PhysicsComponent::update(uint32_t tick_delta) {

	if (velocity_.x != 0 || velocity_.y !=0)
		translate(
				(int)(velocity_.x * tick_delta),
				(int)(velocity_.y * tick_delta));

	constrainIn(bounding_box_);
}

/**
 * Returns the object's current velocity vector
 */
Vector PhysicsComponent::getVelocity()
{
	return velocity_;
}

/**
 * Set the GameObject velocity vector
 */
void PhysicsComponent::setVelocity(Vector velocity)
{
	velocity_ = velocity;
}

/**
 * Moves the GameObject to a pair of absolute coordinates
 */
void PhysicsComponent::moveTo(int x, int y)
{
	box_.x = x;
	box_.y = y;

	game_object_->setBox(box_);
}

/**
 * Translates the GameObject by a given offset pair
 */
void PhysicsComponent::translate(int x, int y)
{
	moveTo(box_.x + x, box_.y + y);
}

/**
 * Constrains a GameObject inside of an SDL_Rect, useful to constrain it
 * inside the screen.
 */
void PhysicsComponent::constrainIn(SDL_Rect rect)
{
	if (box_.x < rect.x)
		box_.x = rect.x;

	else if (box_.x > (rect.w - box_.w))
		box_.x = (rect.w - box_.w);

	if (box_.y < rect.y)
		box_.y = rect.y;

	else if (box_.y > (rect.h - box_.h))
		box_.y = (rect.h - box_.h);

	game_object_->setBox(box_);
}


/**
 * Tests whether two GameObjects are currently box-colliding
 */
bool PhysicsComponent::isBoxCollidingWith(GameObject* other)
{
	SDL_Rect other_box = other->getBox();

	return (box_.x <= (other_box.x + other_box.w)) &&
		((box_.x + box_.w) >= other_box.x) &&
		(box_.y <= (other_box.y + other_box.h)) &&
		((box_.y + box_.h) >= other_box.y);
}


