#ifndef _text_sprite_hpp
#define _text_sprite_hpp

#include <stdio.h>
#include "constants.hpp"
#include "sdl.hpp"
#include "util.hpp"
#include "game_object.hpp"

class TextLabel : public GameObject
{
	public:
		TextLabel(std::string label, std::string font_name, int font_size,
				SDL_Point position, SDL_Renderer* renderer);

		~TextLabel();

		void updateGraphics(uint32_t tick_delta);
		void update(std::string label);

	private:
		static void initTTF();
		void renderTexture();

		std::string label_;
		SDL_Rect rect_;
		TTF_Font* font_;
		SDL_Texture* texture_;
		SDL_Renderer* renderer_;

		static bool ttf_initialized;
};

#endif

