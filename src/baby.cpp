#include "baby.hpp"

static const char* IMG_BABY_SPRITE_SHEET = "Babies.png";
static const int INT_BABY_N_SPRITES_HORIZONTAL = 12;
static const int INT_BABY_N_SPRITES_VERTICAL = 8;
static const int INT_BABY_ANIM_START_WALK_LEFT = 66;
static const int INT_BABY_ANIM_START_WALK_RIGHT = 78;
static const int INT_BABY_ANIM_N_FRAMES = 3;
static const int INT_BABY_ANIM_INTERVAL = 150;
static const int INT_BABY_CEILING_DISTANCE = 25;

Baby::Baby() :
	GameObject(IMG_BABY_SPRITE_SHEET, INT_BABY_N_SPRITES_HORIZONTAL,
			INT_BABY_N_SPRITES_VERTICAL)
{
	sprite_->setFrame(INT_BABY_ANIM_START_WALK_RIGHT);
	physics_component_->moveTo((SCREEN_WIDTH - getBox().w) / 2,
			INT_BABY_CEILING_DISTANCE);
}

Baby::~Baby()
{
}

void Baby::travelTo(int destination_x, uint32_t travel_time)
{
	Vector velocity;

	velocity.y = 0;

	velocity.x = (destination_x - getCenter().x) / travel_time;

	if (velocity.x > 0)
	{
		sprite_->stopAnimation(INT_BABY_ANIM_START_WALK_RIGHT);
		sprite_->startAnimation(INT_BABY_ANIM_START_WALK_RIGHT,
				INT_BABY_ANIM_N_FRAMES,
				0,
				INT_BABY_ANIM_INTERVAL);
	}
	else
	{
		sprite_->stopAnimation(INT_BABY_ANIM_START_WALK_LEFT);
		sprite_->startAnimation(INT_BABY_ANIM_START_WALK_LEFT,
				INT_BABY_ANIM_N_FRAMES,
				0,
				INT_BABY_ANIM_INTERVAL);
	}

	physics_component_->setVelocity(velocity);
}

void Baby::setPosition(int x, int y)
{
	physics_component_->moveTo(x, y);
}

