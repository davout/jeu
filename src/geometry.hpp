#ifndef _geometry_hpp
#define _geometry_hpp

/*
 * Defines a Vector used to record movement speed and direction
 */
typedef struct {
	double x, y;
} Vector;

typedef Vector Point;

typedef struct {
	int w, h;
} Dimensions;

#endif

