#ifndef _player_hpp
#define _player_hpp

#include "constants.hpp"
#include "game_object.hpp"

class Player : public GameObject
{
	public:
		Player();
		~Player();
		void handleInput(const uint8_t* keyboard);
		bool isBoxCollidingWith(GameObject* game_object);
};

#endif

