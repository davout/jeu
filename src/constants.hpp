#ifndef _constants_h
#define _constants_h

#include <stdint.h>
#include "sdl.hpp"

#define INT_MAX_SPRITE_COUNT 0xFF

static const int SCREEN_WIDTH = 1024;
static const int SCREEN_HEIGHT = 768;

// Move speeds
static const double DBL_PLAYER_MOVE_SPEED = 0.5;
static const double DBL_FALL_SPEED = 0.2;

// Toy spawn frequency
static const uint32_t INT_SPAWN_DELAY = 1000;

#endif

