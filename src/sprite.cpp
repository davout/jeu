#include "sprite.hpp"

SDL_Renderer* Sprite::default_renderer = NULL;

/**
 * Creates a sprite from a sheet and a number of frames
 */
Sprite::Sprite(std::string path, int n_frames_horiz, int n_frames_vert) :
	renderer_(default_renderer),
	n_frames_horiz_(n_frames_horiz),
	n_frames_vert_(n_frames_vert)

{
	loadImage(path);
	stopAnimation(0);

	frame_rect_.x = 0;
	frame_rect_.y = 0;
	frame_rect_.h = rect_.h / n_frames_vert_;
	frame_rect_.w = rect_.w / n_frames_horiz_;
}

/**
 * Cleanly destroys a Sprite by freeing its associated SDL_Texture
 */
Sprite::~Sprite()
{
	SDL_DestroyTexture(texture_);
}

/**
 * Loads an image into an SDL_Texture
 */
void Sprite::loadImage(std::string path)
{
	SDL_Surface* surface = IMG_Load(Util::resolveResourcePath(RESOURCE_IMAGE,
				path).c_str());

	if (!surface)
		Util::die("Unable to load image", SDL_GetError());

	texture_ = SDL_CreateTextureFromSurface(renderer_, surface);

	if (!texture_)
		Util::die("Unable to create texture from surface", SDL_GetError());

	rect_.w = surface->w;
	rect_.h = surface->h;

	fprintf(stderr, "Loaded image <%s> with dimensions <%dx%d>\n",
			path.c_str(), rect_.w, rect_.h);

	SDL_FreeSurface(surface);
}

/**
 * Draw the Sprite
 */
void Sprite::draw(SDL_Rect drawing_box)
{
	if (animationInProgress()) setAnimationFrame();
	SDL_RenderCopy(renderer_, texture_, &frame_rect_, &drawing_box);
}

/**
 * Returns the dimensions of the Sprite
 */
Dimensions Sprite::getDimensions()
{
	Dimensions dimensions;

	dimensions.w = frame_rect_.w;
	dimensions.h = frame_rect_.h;

	return dimensions;
}

/**
 * Sets the current frame on a Sprite
 */
void Sprite::setFrame(int frame_idx)
{
	if (frame_idx >= (n_frames_horiz_ * n_frames_vert_))
		Util::die("Out of bounds frame index !");

	current_frame_ = frame_idx;

	frame_rect_.x = frame_rect_.w * (frame_idx % n_frames_horiz_);
	frame_rect_.y = frame_rect_.h * (frame_idx / n_frames_horiz_);
}

/**
 * Returns the currently displayed frame
 */
int Sprite::getFrame()
{
	return current_frame_;
}

/**
 * Returns whether an animation is currently in progress
 */
bool Sprite::animationInProgress()
{
	return animation.loops_ != -1;
}

/**
 * Stops the playing animation, and sets the current frame to `frame_idx`
 */
void Sprite::stopAnimation(int frame_idx)
{
	animation.loops_ = -1;
	setFrame(frame_idx);
}

/**
 * Sets the correct frame according to the current time if an animation is in
 * progress.
 */
void Sprite::setAnimationFrame()
{
	uint32_t since_start, current_ticks, loop_duration;
	int n_loop, n_frame;

	if (!animationInProgress())
	{
		printf("Can't set the animation frame if the sprite isn't animated!");
		exit(1);
	}

	current_ticks = SDL_GetTicks();
	loop_duration = animation.n_frames_ * animation.interval_;
	since_start = current_ticks - animation.start_tick_;
	n_loop = since_start / loop_duration;

	if (animation.loops_ == 0 || (n_loop < animation.loops_))
	{
		n_frame = ((since_start % loop_duration) / animation.interval_) +
			animation.start_frame_;

		setFrame(n_frame);
	}
}

/**
 * Starts the sprite animation from frame `start_frame`, for `n_frames` with an
 * `interval` ms interval between each frame.
 */
void Sprite::startAnimation(int start_frame, int n_frames, int loops,
		uint32_t interval)
{
	if (n_frames <= 1)
	{
		printf("We can't animate less than two frames!");
		exit(1);
	}

	if (!animationInProgress())
	{
		animation.start_frame_	= start_frame;
		animation.n_frames_		= n_frames;
		animation.loops_		= loops;
		animation.start_tick_	= SDL_GetTicks();
		animation.interval_		= interval;
	}
}


