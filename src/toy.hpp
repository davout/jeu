#ifndef _toy_hpp
#define _toy_hpp

#include "constants.hpp"
#include "game_object.hpp"

class Toy : public GameObject
{
	public:
		Toy(int x);
		~Toy();
};

#endif

