#ifndef _baby_hpp
#define _baby_hpp

#include "view.hpp"
#include "game_object.hpp"

class Baby : public GameObject
{
	public:
		Baby();
		~Baby();

		void travelTo(int destination_x, uint32_t travel_time);
		void setPosition(int x, int y);
};

#endif

