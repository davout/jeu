#ifndef _game_object_hpp
#define _game_object_hpp

#include "constants.hpp"
#include "sprite.hpp"
#include "geometry.hpp"
#include "physics_component.hpp"

class GameObject
{

	public:
		GameObject(std::string sprite_sheet_path, int n_frames_horiz,
				int n_frames_vert);

		GameObject();
		virtual ~GameObject();

		/* void draw(); */
		SDL_Rect getBox();
		void setBox(SDL_Rect box);
		Point getCenter();

		// Component updates
		virtual void updatePhysics(uint32_t tick_delta);
		virtual void updateGraphics(uint32_t tick_delta);

	protected:
		SDL_Rect box_;
		Sprite* sprite_;

		PhysicsComponent* physics_component_;

		void setSprite(Sprite* sprite);
		void initPhysicsComponent();
};

#endif

