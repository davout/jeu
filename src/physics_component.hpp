#ifndef _physics_component_hpp
#define _physics_component_hpp

#include "sdl.hpp"
#include "geometry.hpp"

class GameObject;

class PhysicsComponent
{
	public:
		PhysicsComponent(GameObject* game_object, SDL_Rect bounding_box);
		~PhysicsComponent();

		void update(uint32_t tick_delta);

		Vector getVelocity();
		void setVelocity(Vector velocity);
		void moveTo(int x, int y);
		void translate(int x, int y);
		void constrainIn(SDL_Rect rect);
		bool isBoxCollidingWith(GameObject* other);

	private:
		GameObject* game_object_;
		SDL_Rect box_;

		// "World" box in which physical evolutions are constrained
		SDL_Rect bounding_box_;

		Vector velocity_;
};

#endif

