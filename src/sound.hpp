#ifndef _sound_h
#define _sound_h

#include "sdl.hpp"
#include "util.hpp"
#include "constants.hpp"

void Sound_Init();
Mix_Chunk* Sound_Load(const char* sound_path);
Mix_Music* Sound_LoadMusic(const char* sound_path);
void Sound_Play(Mix_Chunk* sound);
void Sound_PlayMusic(Mix_Music* sound);

#endif

