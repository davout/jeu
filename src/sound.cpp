#include "sound.hpp"

void Sound_Init()
{
	int result,
		flags = MIX_INIT_OGG;

	if (flags != (result = Mix_Init(flags)))
	{
		printf("Could not initialize mixer (result: %d).\n", result);
		printf("Mix_Init: %s\n", Mix_GetError());
		exit(1);
	}

	Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 4096);
	Mix_VolumeMusic(MIX_MAX_VOLUME);
}

Mix_Chunk* Sound_Load(const char* sound_path)
{
	Mix_Chunk *sound = Mix_LoadWAV(Util::resolveResourcePath(RESOURCE_SFX,
				sound_path).c_str());

	if (!sound)
	{
		printf("Failed to play music: %s\n", Mix_GetError());
		exit(1);
	}

	return sound;
}

Mix_Music* Sound_LoadMusic(const char* sound_path)
{
	Mix_Music *sound = Mix_LoadMUS(Util::resolveResourcePath(RESOURCE_MUSIC,
				sound_path).c_str());

	if (!sound)
	{
		printf("Failed to play music: %s\n", Mix_GetError());
		exit(1);
	}

	return sound;
}

void Sound_PlayMusic(Mix_Music* sound)
{
	Mix_PlayMusic(sound, -1);
}

void Sound_Play(Mix_Chunk* sound)
{
	Mix_PlayChannel(-1, sound, 0);
}

