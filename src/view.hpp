#ifndef _view_c
#define _view_c

#include "sdl.hpp"
#include "constants.hpp"
#include "sprite.hpp"
#include "game_object.hpp"
#include "text_label.hpp"

class View
{
	public:
		View();
		~View();
		void createBaseWindow();
		void reset();
		void show();
		SDL_Rect getBox();
		void renderText(char* text, int text_x, int text_y);
		SDL_Renderer* getRenderer();

	private:
		void renderScore();

		SDL_Renderer* renderer_;
		SDL_Window* window_;
		Sprite*	background_;
		SDL_Rect box_;
		uint32_t score_;
		TextLabel* score_label_;
};

#endif

