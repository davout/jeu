#include "game_object.hpp"

/**
 * Initializes a GameObject
 */
GameObject::GameObject()
{
	// plox to init physics
	physics_component_ = NULL;
	sprite_ = NULL;
}

/**
 * Initializes a GameObject given a sprite sheet (along with its dimensions).
 */
GameObject::GameObject(std::string sprite_sheet_path, int n_frames_horiz,
		int n_frames_vert)
{
	physics_component_ = NULL;
	setSprite(new Sprite(sprite_sheet_path, n_frames_horiz, n_frames_vert));
}

void GameObject::initPhysicsComponent()
{
	if (!physics_component_)
	{
		SDL_Rect bounding_box;

		bounding_box.x = 0;
		bounding_box.y = 0;
		bounding_box.h = SCREEN_HEIGHT;
		bounding_box.w = SCREEN_WIDTH;

		physics_component_ = new PhysicsComponent(this, bounding_box);
	}
}

/**
 * Destroys a GameObject
 */
GameObject::~GameObject()
{
	if (physics_component_)
		delete physics_component_;

	if (sprite_)
		delete sprite_;
}

/**
 * Draws the GameObject's sprite in `box_`
 */
/* void GameObject::draw() */
/* { */
/* 	sprite_->draw(box_); */
/* } */

/**
 * Sets the GO's box from the supplied parameter
 */
void GameObject::setBox(SDL_Rect box)
{
	box_ = box;
}

/**
 * Returns the GameObject's box as an SDL_Rect
 */
SDL_Rect GameObject::getBox()
{
	return box_;
}

/**
 * Get the GameObject center
 */
Point GameObject::getCenter()
{
	Point p;

	p.x = box_.x + box_.w / 2;
	p.y = box_.y + box_.h / 2;

	return p;
}

void GameObject::updatePhysics(uint32_t tick_delta)
{
	if (physics_component_)
		physics_component_->update(tick_delta);
}

void GameObject::updateGraphics(uint32_t tick_delta)
{
	sprite_->draw(box_);
}

void GameObject::setSprite(Sprite* sprite)
{
	Dimensions sprite_dimensions = sprite->getDimensions();

	sprite_ = sprite;

	box_.x = 0;
	box_.y = 0;
	box_.w = sprite_dimensions.w;
	box_.h = sprite_dimensions.h;

	initPhysicsComponent();
}

