#ifndef _sprite_h
#define _sprite_h

#include <stdio.h>
#include "sdl.hpp"
#include "geometry.hpp"
#include "util.hpp"

class Sprite {

	public:
		Sprite(std::string path, int n_frames_horiz, int n_frames_vert);
		~Sprite();

		void loadImage(std::string path);
		void draw(SDL_Rect drawing_box);

		Dimensions getDimensions();

		// Animation-related methods
		void setFrame(int frame_idx);
		int getFrame();
		bool animationInProgress();
		void stopAnimation(int frame_idx);
		void setAnimationFrame();
		void startAnimation(int start_frame, int n_frames, int loops,
				uint32_t interval);

		static SDL_Renderer* default_renderer;


	private:
		// Sprite texture
		SDL_Texture* texture_;

		// Rectangle for the whole texture
		SDL_Rect rect_;

		// Rectangle for the current frame
		SDL_Rect frame_rect_;

		// A reference to the renderer this Sprite is associated to
		SDL_Renderer* renderer_;

		int n_frames_horiz_, n_frames_vert_, current_frame_;

		struct {
			int start_frame_, n_frames_, loops_;
			uint32_t start_tick_, interval_;
		} animation;
};

#endif

