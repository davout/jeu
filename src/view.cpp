#include "view.hpp"

// Background
static const char* IMG_MAIN_BACKGROUND = "background.png";

// Game icon
static const char* IMG_GAME_ICON = "game-icon.png";

// Main window title
static const char* GAME_TITLE = "Sister attack";

/**
 * Initialize a new View
 */
View::View()
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
		Util::die("SDL could not initialize", SDL_GetError());

	createBaseWindow();

	box_.x = 0;
	box_.y = 0;
	box_.w = SCREEN_WIDTH;
	box_.h = SCREEN_HEIGHT;

	renderer_ = SDL_CreateRenderer(window_, -1,
			SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	// HACK: sprites need this when initially loading their textures
	Sprite::default_renderer = renderer_;

	if (!renderer_)
		Util::die("Unable to create renderer", SDL_GetError());

	// Initialize the background image
	background_ = new Sprite(IMG_MAIN_BACKGROUND, 1, 1);

	// Set the window icon
	SDL_Surface *icon = IMG_Load(IMG_GAME_ICON);
	SDL_SetWindowIcon(window_, icon);
}

/**
 * Cleanly destroys a view
 */
View::~View()
{
	SDL_DestroyRenderer(renderer_);
	delete background_;
}

/*
 * Create the game window for this View
 */
void View::createBaseWindow()
{
	window_ = SDL_CreateWindow(GAME_TITLE, SDL_WINDOWPOS_UNDEFINED,
			SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT,
			SDL_WINDOW_SHOWN);

	if (!window_)
		Util::die("Window could not be created", SDL_GetError());
}

/**
 * Draw the view, starting from the background
 */
/* void View::draw(GameObject* game_objects[], int n_game_objects) */
void View::reset()
{
	SDL_RenderClear(renderer_);
	background_->draw(box_);
}

void View::show()
{
	SDL_RenderPresent(renderer_);
}

SDL_Rect View::getBox()
{
	return box_;
}

/**
 * Returns the currently used SDL_Renderer
 */
SDL_Renderer* View::getRenderer()
{
	return renderer_;
}

