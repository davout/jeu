#ifndef _util_hpp
#define _util_hpp

#ifdef __FUCKYOU__
#include "CoreFoundation/CoreFoundation.h"
#endif

#include <iostream>
#include <stdlib.h>
#include <string>

#define BASE_PATH_MAX 1000

typedef enum {
	RESOURCE_IMAGE,
	RESOURCE_MUSIC,
	RESOURCE_SFX,
	RESOURCE_FONT
} ResourceType;

class Util
{
	public:
	static std::string resolveResourcePath(ResourceType type, std::string name);
	static void die(std::string str);
	static void die(std::string str, const char* detail);

	private:
	Util();
	~Util();

	static std::string getBaseResourcePath();
};

#endif

