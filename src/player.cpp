#include "player.hpp"

// Player character
static const char* IMG_PLAYER_SPRITE_SHEET= "player-sprite-sheet.png";
static const int INT_PLAYER_N_SPRITES_HORIZONTAL = 12;
static const int INT_PLAYER_N_SPRITES_VERTICAL = 4;
static const int INT_PLAYER_REST_RIGHT_FRAME = 9;
static const int INT_PLAYER_REST_LEFT_FRAME = 9;
static const int INT_PLAYER_REST_MIDDLE_FRAME = 9;
static const int INT_PLAYER_ANIM_START_WALK_LEFT = 12;
static const int INT_PLAYER_ANIM_N_FRAMES_WALK_LEFT = 12;
static const int INT_PLAYER_ANIM_INTERVAL_WALK_LEFT = 70;
static const int INT_PLAYER_ANIM_START_WALK_RIGHT = 24;
static const int INT_PLAYER_ANIM_N_FRAMES_WALK_RIGHT = 12;
static const int INT_PLAYER_ANIM_INTERVAL_WALK_RIGHT = 70;

static const int INT_PLAYER_FLOOR_DISTANCE = 25;

Player::Player() :
	GameObject(IMG_PLAYER_SPRITE_SHEET, INT_PLAYER_N_SPRITES_HORIZONTAL,
			INT_PLAYER_N_SPRITES_VERTICAL)
{
	Dimensions player_size = sprite_->getDimensions();

	physics_component_->moveTo((SCREEN_WIDTH - player_size.w) / 2,
			SCREEN_HEIGHT - player_size.h - INT_PLAYER_FLOOR_DISTANCE);
}

Player::~Player()
{
}

void Player::handleInput(const uint8_t* keyboard)
{
	static int walk_direction = 0;
	Vector velocity;
	velocity.y = 0;

	if (keyboard[SDL_SCANCODE_LEFT])
	{
		velocity.x = -DBL_PLAYER_MOVE_SPEED;

		if (walk_direction == 1)
			sprite_->stopAnimation(INT_PLAYER_ANIM_START_WALK_RIGHT);

		sprite_->startAnimation(INT_PLAYER_ANIM_START_WALK_LEFT,
				INT_PLAYER_ANIM_N_FRAMES_WALK_LEFT,
				0,
				INT_PLAYER_ANIM_INTERVAL_WALK_LEFT);

		walk_direction = -1;
	}
	else if (keyboard[SDL_SCANCODE_RIGHT])
	{
		velocity.x = DBL_PLAYER_MOVE_SPEED;

		if (walk_direction == -1)
			sprite_->stopAnimation(INT_PLAYER_ANIM_START_WALK_RIGHT);

		sprite_->startAnimation(INT_PLAYER_ANIM_START_WALK_RIGHT,
				INT_PLAYER_ANIM_N_FRAMES_WALK_RIGHT,
				0,
				INT_PLAYER_ANIM_INTERVAL_WALK_RIGHT);

		walk_direction = 1;
	}
	else if (velocity.x != 0)
	{
		velocity.x = 0;
		walk_direction = 0;

		if (velocity.x < 0.0)
			sprite_->stopAnimation(INT_PLAYER_REST_LEFT_FRAME);
		else
			sprite_->stopAnimation(INT_PLAYER_REST_RIGHT_FRAME);
	}

	physics_component_->setVelocity(velocity);
}

bool Player::isBoxCollidingWith(GameObject* game_object)
{
	return physics_component_->isBoxCollidingWith(game_object);
}
