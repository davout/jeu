#include "util.hpp"

/**
 * Returns the path to a resource given its type and filename
 */
std::string Util::resolveResourcePath(ResourceType type, std::string name)
{
	static std::string base_path = getBaseResourcePath();
	std::string type_str;

	switch(type)
	{
		case RESOURCE_IMAGE:
			type_str = "images";
			break;

		case RESOURCE_SFX:
			type_str = "sfx";
			break;

		case RESOURCE_MUSIC:
			type_str = "music";
			break;

		case RESOURCE_FONT:
			type_str = "fonts";
			break;

		default:
			printf("Unhandled resource type.\n");
			exit(1);
	}

	return base_path + "/" + type_str + "/" + name;

}

/**
 * Exits the program with an error message
 */
void Util::die(std::string str)
{
	std::cout << str << std::endl;
	exit(1);
}

/**
 * Exits the program with an error message and error details
 */
void Util::die(std::string str, const char* detail)
{
	std::cout << str << ": " << detail << std::endl;
	exit(1);
}

/**
 * Returns the base resource path according to the environment we've built in
 */
std::string Util::getBaseResourcePath()
{
#ifdef __FUCKYOU__
	CFBundleRef main_bundle = CFBundleGetMainBundle();
    CFURLRef resources_url = CFBundleCopyResourcesDirectoryURL(main_bundle);
    char path[BASE_PATH_MAX];

    if (!CFURLGetFileSystemRepresentation(resources_url, TRUE,
				(char*)path, BASE_PATH_MAX))
    {
		printf("Unable to get base path.\n");
		exit(1);
    }

    CFRelease(resources_url);

	return std::string(path);
#else
	return std::string("assets");
#endif
}

