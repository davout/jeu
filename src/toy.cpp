#include "toy.hpp"

// Toys
#define N_TOYS 4
static const char* IMG_TOY_01 = "toy_01.png";
static const char* IMG_TOY_02 = "toy_02.png";
static const char* IMG_TOY_03 = "toy_03.png";
static const char* IMG_TOY_04 = "toy_04.png";

static const char* TOY_LIST[N_TOYS] = {
	IMG_TOY_01,
	IMG_TOY_02,
	IMG_TOY_03,
	IMG_TOY_04
};

Toy::Toy(int x)
{
	setSprite(new Sprite(TOY_LIST[rand() % N_TOYS], 1, 1));

	Dimensions toy_size = sprite_->getDimensions();
	Vector velocity = physics_component_->getVelocity(); 

	physics_component_->moveTo(x - toy_size.w, -toy_size.h);

	velocity.y = DBL_FALL_SPEED;
	physics_component_->setVelocity(velocity);
}

Toy::~Toy()
{
}

